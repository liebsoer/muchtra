'use strict';

var fs = require('fs');
var mkdirp = require('mkdirp');
var Q = require('Q');
var home = process.env[process.platform === 'win32' ? 'USERPROFILE' : 'HOME'].replace(/\\/g, "/");

var dataDir = home + '/.muchtra/';
var resultsDir = dataDir + 'results/';
var filePaths = {
    category: dataDir + 'categories.json',
    login: dataDir + 'logins.json',
    question: dataDir + 'questions.json',
    set: dataDir + 'set.json',
    user: dataDir + 'user.json'
};

try {
    mkdirp.sync(dataDir);
    mkdirp.sync(resultsDir);
} catch (e) {
    console.error(e);
}

// Helper functions

function fs_readFile(file, encoding) {
    var deferred = Q.defer();
    encoding = encoding || 'utf-8';
    fs.readFile(file, encoding, function (err, data) {
        if (err) {
            deferred.reject(err); // rejects the promise with `err` as the reason
        } else {
                deferred.resolve(JSON.parse(data)); // fulfills the promise with `data` as the value
            }
    });
    return deferred.promise; // the promise is returned
}

// Login
function writeLoginFile(logins) {

    var deffered = Q.defer();
    if (logins && logins.constructor && logins.constructor === Object) {
        fs.writeFile(filePaths.login, JSON.stringify(logins), function (err) {
            if (err) {
                deffered.reject(err);
            }
            deffered.resolve(true);
        });
    } else {
        console.warn('Logins are not written because of wrong format');
        deffered.reject(new Error('Wrong format'));
    }
    return deffered.promise;
}

function getLoginFile() {
    var deffered = Q.defer();
    fs_readFile(filePaths.login).then(function (data) {
        deffered.resolve(data);
    }, function (err) {
        console.warn('Error while fetching login file. Default returned.', err);
        deffered.resolve({
            users: {},
            tokens: {}
        });
    });
    return deffered.promise;
}
exports.login = {
    writeLogin: writeLoginFile,
    getLogin: getLoginFile
};

// User
function writeUserFile(users) {
    var deffered = Q.defer();
    if (users && users.constructor && users.constructor === Object) {
        fs.writeFile(filePaths.user, JSON.stringify(users), function (err) {
            if (err) {
                deffered.reject(err);
                return;
            }
            deffered.resolve(true);
        });
    } else {
        console.warn('Users are not written because of wrong format');
        deffered.reject(new Error('Wrong format'));
    }
    return deffered.promise;
}

function getUserFile() {
    var deffered = Q.defer();
    fs_readFile(filePaths.user).then(function (data) {
        deffered.resolve(data);
    }, function (err) {
        console.warn('Error while fetching user file. Default returned.', err);
        deffered.resolve({});
    });
    return deffered.promise;
}

exports.user = {
    writerUsers: writeUserFile,
    getUsers: getUserFile
};

// Category
function getCategories() {
    var deffered = Q.defer();
    fs_readFile(filePaths.category).then(function (data) {
        deffered.resolve(data);
    }, function (err) {
        console.log('Error while fetching category file. Default returned', err);
        deffered.resolve([]);
    });
    return deffered.promise;
}

function writeCategories(categories) {
    var deffered = Q.defer();
    if (categories && categories.constructor && categories.constructor === Array) {
        fs.writeFile(filePaths.category, JSON.stringify(categories), function (err) {
            if (err) {
                deffered.reject(err);
                return;
            }
            deffered.resolve(true);
        });
    } else {
        console.warn('Categories are not written because of wrong format');
        deffered.reject(new Error('Wrong format'));
    }
    return deffered.promise;
}

function isHasCategory(categoryName) {
    var deffered = Q.defer();
    getCategories().then(function (categories) {
        for (var i = 0; i < categories.length; i++) {
            if (categories[i].name === categoryName) {
                deffered.resolve(true);
                return;
            }
        }
        deffered.resolve(false);
    }, function (err) {
        console.warn(err);
        deffered.resolve(false);
    });
    return deffered.promise;
}
exports.category = {
    getCategories: getCategories,
    isHasCategory: isHasCategory,
    writeCategories: writeCategories
};

// Set

function getSets() {
    var deffered = Q.defer();
    fs_readFile(filePaths.set).then(function (data) {
        deffered.resolve(data);
    }, function () {
        console.warn('Error while fetching sets file. Default returned');
        deffered.resolve([]);
    });
    return deffered.promise;
}

function isHasSet(setName) {
    var deffered = Q.defer();
    getSets().then(function (sets) {
        for (var i = 0; i < sets.length; i++) {
            if (sets[i].name === setName) {
                deffered.resolve(true);
                return;
            }
        }
        deffered.resolve(false);
    }, function (err) {
        console.warn(err);
        deffered.resolve(false);
    });
    return deffered.promise;
}

function writeSets(sets) {
    var deffered = Q.defer();
    if (sets && sets.constructor && sets.constructor === Array) {
        fs.writeFile(filePaths.set, JSON.stringify(sets), function (err) {
            if (err) {
                deffered.reject(err);
                return;
            }
            deffered.resolve(true);
        });
    } else {
        console.warn('Sets are not written because of wrong format');
        deffered.reject(new Error('Wrong format'));
    }
    return deffered.promise;
}
exports.set = {
    getSets: getSets,
    writeSets: writeSets,
    isHasSet: isHasSet
};

// Question
function getQuestions() {
    var deffered = Q.defer();
    fs_readFile(filePaths.question).then(function (data) {
        deffered.resolve(data);
    }, function () {
        console.warn('Error while fetching questions file. Default returned');
        deffered.resolve([]);
    });
    return deffered.promise;
}

function isHasQuestion(question) {
    var deffered = Q.defer();
    getQuestions().then(function (questions) {
        for (var i = 0; i < questions.length; i++) {
            if (questions[i].question === question) {
                deffered.resolve(true);
                return;
            }
        }
        deffered.resolve(false);
    }, function (err) {
        console.warn(err);
        deffered.resolve(false);
    });
    return deffered.promise;
}

function writeQuestions(questions) {
    var deffered = Q.defer();
    if (questions && questions.constructor && questions.constructor === Array) {
        fs.writeFile(filePaths.question, JSON.stringify(questions), function (err) {
            if (err) {
                deffered.reject(err);
                return;
            }
            deffered.resolve(true);
        });
    } else {
        console.warn('Questions are not written because of wrong format');
        deffered.reject(new Error('Wrong Format'));
    }
    return deffered.promise;
}

exports.question = {
    getQuestions: getQuestions,
    isHasQuestion: isHasQuestion,
    writeQuestions: writeQuestions
};

// Results

function getResults(user) {
    var deffered = Q.defer();

    fs_readFile(resultsDir + user + '.json').then(function (data) {
        deffered.resolve(data);
    }, function () {
        console.warn('Error while fetching result file for ' + user + '. Default returned');
        deffered.resolve({});
    });

    return deffered.promise;
}

function writeResults(user, results) {
    var deffered = Q.defer();

    if (user && results && results.constructor && results.constructor === Object) {
        fs.writeFile(resultsDir + user + '.json', JSON.stringify(results), function (err) {
            if (err) {
                deffered.reject(err);
                return;
            }
            deffered.resolve(true);
        });
    } else {
        console.warn('Results are not written because of wrong format');
        deffered.reject(new Error('Wrong Format'));
    }
    return deffered.promise;
}

exports.results = {
    getResults: getResults,
    writeResults: writeResults
};
//# sourceMappingURL=index.js.map
