/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/login              ->  index
 */

'use strict';
var db = require('../../components/db');
var crypto = require('crypto');
var base64url = require('base64url');
var Q = require('Q');
var secret = 'null';

function creatLoginToken() {
    return base64url(crypto.randomBytes(32));
}

function createPasswordHash(pw) {
    return crypto.createHmac('sha512', secret).update(pw).digest('hex');
}

exports.doPostLogin = function (req, res) {
    db.login.getLogin().then(function (logins) {
        db.user.getUsers().then(function (users) {
            var user = req.body.username;
            var pw = req.body.password;

            if (!user || !pw) {
                console.log('User or pw not provided', user, pw);
                res.status(401);
                res.json({
                    data: null,
                    message: 'Username or password is wrong',
                    status: 'error'
                });
                return;
            }

            var pwHash = createPasswordHash(pw);
            if (!users[user] || users[user] !== pwHash) {
                if (logins.users && logins.users[user]) {
                    delete logins.users[user];
                }
                for (var i = 0; logins.tokens && i < logins.tokens.length; i++) {
                    if (logins.users && logins.users[logins.tokens[i]]) {
                        delete logins.users[logins.tokens[i]];
                    }
                }

                res.status(401);
                res.json({
                    data: null,
                    message: 'Username or password is wrong',
                    status: 'error'
                });
                return;
            }

            var token = creatLoginToken();
            var valid = new Date(Date.now + 86400000);
            logins.users[user] = {
                token: token,
                valid: valid
            };
            logins.tokens[token] = user;

            db.login.writeLogin(logins).then(function () {
                res.status(200);

                res.cookie('muchtra-login', token, {
                    httpOnly: true,
                    maxAge: 86400000
                });

                res.cookie('muchtra-user', user, {
                    httpOnly: true,
                    maxAge: 86400000
                });

                res.json({
                    data: null,
                    message: 'Logged in',
                    status: 'success'
                });
            }, function (err) {
                res.status(500);
                res.json({
                    data: err,
                    message: err.message,
                    status: 'error'
                });
            });
        });
    });
};

exports.doPostRegister = function (req, res) {
    db.login.getLogin().then(function (logins) {
        db.user.getUsers().then(function (users) {
            var user = req.body.username;
            var pw = req.body.password;

            if (!user || !pw) {
                res.status(409);
                res.json({
                    data: null,
                    message: 'Username or password missing',
                    status: 'error'
                });
                return;
            }

            if (users[user]) {
                res.status(409);
                res.json({
                    data: null,
                    message: 'User already exists!',
                    status: 'error'
                });
                return;
            }

            users[user] = createPasswordHash(pw);

            var token = creatLoginToken();
            var valid = new Date(Date.now() + 86400000);
            logins.users[user] = {
                token: token,
                valid: valid
            };
            logins.tokens[token] = user;

            db.login.writeLogin(logins).then(function () {
                db.user.writerUsers(users).then(function () {
                    res.cookie('muchtra-login', token, {
                        httpOnly: true,
                        maxAge: 86400000
                    });

                    res.status(200);
                    res.json({
                        data: null,
                        message: 'Account created',
                        status: 'success'
                    });
                }, function (err) {
                    res.status(500);
                    res.json({
                        data: err,
                        message: err.message,
                        status: 'error'
                    });
                });
            }, function (err) {
                res.status(500);
                res.json({
                    data: err,
                    message: err.message,
                    status: 'error'
                });
            });
        }, function (err) {
            res.status(500);
            res.json({
                data: err,
                message: err.message,
                status: 'error'
            });
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};

exports.doPostLogout = function (req, res) {
    var token = req.cookies['muchtra-login'];
    if (token) {
        console.log('token', token);
        db.login.getLogin().then(function (logins) {
            console.log('Logins received', logins.tokens[token]);
            if (logins.tokens[token]) {
                var user = logins.tokens[token];
                delete logins.tokens[token];
                if (logins.users[user]) {
                    delete logins.users[user];
                }
            }

            db.login.writeLogin(logins).then(function () {
                res.status(200);
                res.json({
                    data: null,
                    message: 'User logged out',
                    status: 'success'
                });
            }, function (err) {
                console.error('Error while writing login file', err);
                res.status(500);
                res.json({
                    data: err,
                    message: err.message,
                    status: 'error'
                });
            });
        }, function (err) {
            console.log('error while receiving logins', err);
            res.status(500);
            res.json({
                data: err,
                message: err.message,
                status: 'error'
            });
        });
    } else {
        res.status(500);
        res.json({
            data: new Error('No login token availabel'),
            message: 'No login token availabel',
            status: 'error'
        });
    }
};

exports.isAuthorized = function (token) {
    var deffered = Q.defer();
    db.login.getLogin().then(function (logins) {
        if (logins.tokens[token] && logins.users[logins.tokens[token]].token === token) {
            deffered.resolve(true);
        } else {
            deffered.resolve(false);
        }
    }, function (err) {
        console.warn(err);
        deffered.resolve(false);
    });
    return deffered.promise;
};
//# sourceMappingURL=login.controller.js.map
