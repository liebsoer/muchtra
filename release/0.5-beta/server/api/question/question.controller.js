/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/question              ->  index
 */

'use strict';

var db = require('../../components/db');

exports.addOrUpdateQuestion = function (req, res) {
    db.question.getQuestions().then(function (questions) {
        var question = req.params.question;
        var oldQuestion = req.body.data.oldQuestion;
        var answers = req.body.data.answers;
        var correctAnswer = req.body.data.correctAnswer;
        var sets = req.body.data.sets;
        var type = req.body.data.type;

        if (question) {
            if (!oldQuestion) {
                db.question.isHasQuestion(question).then(function (exists) {
                    if (exists) {
                        res.status(409);
                        res.json({
                            data: null,
                            message: 'Question \"' + question + '\" already existing!',
                            status: 'error'
                        });
                        return;
                    }

                    questions.push({
                        answers: answers,
                        correctAnswer: correctAnswer,
                        question: question,
                        sets: sets || [],
                        type: type
                    });
                    db.question.writeQuestions(questions).then(function () {
                        res.json({
                            data: null,
                            message: 'Question \"' + question + '\" created',
                            status: 'success'
                        });
                    }, function (err) {
                        res.status(500);
                        res.json({
                            data: err,
                            message: err.message,
                            status: 'error'
                        });
                    });
                }, function (err) {
                    res.status(500);
                    res.json({
                        data: err,
                        message: err.message,
                        status: 'error'
                    });
                });
            } else {
                var message = '';
                for (var i = 0; i < questions.length; i++) {
                    if (question !== oldQuestion && questions[i].question === oldQuestion) {
                        questions.splice(i, 1);
                        questions.push({
                            answers: answers,
                            correctAnswer: correctAnswer,
                            question: question,
                            sets: sets || [],
                            type: type
                        });
                        message = 'Question \"' + oldQuestion + ' renamed to \"' + question + '\"';
                        break;
                    } else if (questions[i].question === question) {
                        questions[i].answers = answers;
                        questions[i].correctAnswer = correctAnswer;
                        questions[i].question = question;
                        questions[i].sets = sets;
                        questions[i].type = type;
                        message = 'Question \"' + question + '\" updated';
                        break;
                    }
                }
                db.question.writeQuestions(questions).then(function () {
                    res.json({
                        data: null,
                        message: message,
                        status: 'success'
                    });
                }, function (err) {
                    res.status(500);
                    res.json({
                        data: err,
                        message: err.message,
                        status: 'error'
                    });
                });
            }
        } else {
            res.status(412);
            res.json({
                data: null,
                message: 'No question available',
                status: 'error'
            });
        }
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};

exports.deleteQuestion = function (req, res) {
    var question = req.params.question;

    if (!question) {
        res.status(412);
        res.json({
            data: null,
            message: 'No question question available',
            status: 'error'
        });
        return;
    }

    if (question.toLowerCase() === 'default') {
        res.status(405);
        res.json({
            data: null,
            message: 'Default question is not allowed to be deleted!',
            status: 'error'
        });
        return;
    }

    db.question.getQuestions().then(function (questions) {
        var success = function success() {
            res.status(200);
            res.json({
                data: null,
                message: 'Question \"' + question + '\" succesfully deleted.',
                status: 'success'
            });
        };
        var failure = function failure(err) {
            res.status(500);
            res.json({
                data: err,
                message: err.message,
                status: 'error'
            });
        };

        for (var i = 0; i < questions.length; i++) {
            if (questions[i].question === question) {
                questions.splice(i, 1);
                db.question.writeQuestions(questions).then(success, failure);
                return;
            }
        }

        res.status(410);
        res.json({
            data: null,
            message: 'Question \"' + question + '\" was not found. Nothing was deleted.',
            status: 'error'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};

exports.getQuestions = function (req, res) {
    db.question.getQuestions().then(function (questions) {
        res.status(200);
        res.json({
            data: questions,
            message: null,
            satus: 'success'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};

exports.isHasQuestion = function (req, res) {
    db.question.isHasQuestion(req.params.question).then(function (exists) {
        res.status(200);
        res.json({
            data: exists,
            message: 'Question ' + (exists ? '' : 'does not ') + 'exists',
            status: 'success'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};
//# sourceMappingURL=question.controller.js.map
