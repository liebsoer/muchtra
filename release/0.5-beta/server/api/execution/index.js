'use strict';

module.exports = function (app) {
    var express = require('express');
    var controller = require('./execution.controller');

    var router = express.Router();

    router.get('/', controller.getExecutionData);
    router.post('/', controller.initTestRun);
    router.put('/:id', controller.persistTestRun);
    router.get('/question', controller.getQuestionData);
    router.get('/result', controller.getOverviewData);
    router.get('/result/:id', controller.getResultData);

    return router;
};
//# sourceMappingURL=index.js.map
