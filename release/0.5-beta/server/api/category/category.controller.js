/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/category              ->  index
 */

'use strict';

var db = require('../../components/db');

exports.addOrUpdateCategory = function (req, res) {
    db.category.getCategories().then(function (categories) {
        var name = req.params.category;
        var oldName = req.body.data.oldName;
        var description = req.body.data.description;
        if (name) {
            if (!oldName) {
                db.category.isHasCategory(name).then(function (exists) {
                    if (exists) {
                        res.status(409);
                        res.json({
                            data: null,
                            message: 'Category \"' + name + '\" already existing!',
                            status: 'error'
                        });
                        return;
                    }

                    categories.push({
                        name: name,
                        description: description || ''
                    });

                    db.category.writeCategories(categories).then(function () {
                        res.json({
                            data: null,
                            message: 'Category \"' + name + '\" created',
                            status: 'success'
                        });
                    }, function (err) {
                        res.status(500);
                        res.json({
                            data: err,
                            message: err.message,
                            status: 'error'
                        });
                    });
                }, function (err) {
                    res.status(500);
                    res.json({
                        data: err,
                        message: err.message,
                        status: 'error'
                    });
                });
            } else {
                var message = '';
                for (var i = 0; i < categories.length; i++) {
                    if (name !== oldName && categories[i].name === oldName) {
                        categories.splice(i, 1);
                        categories.push({
                            name: name,
                            description: description || ''
                        });
                        message = 'Category \"' + oldName + ' renamed to \"' + name + '\"';
                        break;
                    } else if (categories[i].name === name) {
                        categories[i].name = name;
                        categories[i].description = description;
                        message = 'Category \"' + name + '\" updated';
                        break;
                    }
                }
                db.category.writeCategories(categories).then(function () {
                    res.json({
                        data: null,
                        message: message,
                        status: 'success'
                    });
                }, function (err) {
                    res.status(500);
                    res.json({
                        data: err,
                        message: err.message,
                        status: 'error'
                    });
                });
            }
        } else {
            res.status(412);
            res.json({
                data: null,
                message: 'No category name available',
                status: 'error'
            });
        }
    });
};

exports.deleteCategory = function (req, res) {
    var category = req.params.category;

    if (!category) {
        res.status(412);
        res.json({
            data: null,
            message: 'No category name available',
            status: 'error'
        });
        return;
    }

    if (category.toLowerCase() === 'default') {
        res.status(405);
        res.json({
            data: null,
            message: 'Default category is not allowed to be deleted!',
            status: 'error'
        });
        return;
    }

    db.category.getCategories().then(function (categories) {
        var success = function success() {
            res.status(200);
            res.json({
                data: null,
                message: 'Category \"' + category + '\" succesfully deleted.',
                status: 'success'
            });
        };
        var failure = function failure(err) {
            res.status(500);
            res.json({
                data: err,
                message: err.message,
                status: 'error'
            });
        };

        for (var i = 0; i < categories.length; i++) {
            if (categories[i].name === category) {

                categories.splice(i, 1);

                db.category.writeCategories(categories).then(success, failure);
                return;
            }
        }

        res.status(410);
        res.json({
            data: null,
            message: 'Category \"' + category + '\" was not found. Nothing was deleted.',
            status: 'error'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};

exports.getCategories = function (req, res) {
    db.category.getCategories().then(function (categories) {
        res.json({
            data: categories,
            message: null,
            satus: 'success'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};

exports.isHasCategory = function (req, res) {
    db.category.isHasCategory(req.params.category).then(function (exists) {
        res.status(200);
        res.json({
            data: exists,
            message: 'Category ' + (exists ? '' : 'does not ') + 'exists',
            status: 'success'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};
//# sourceMappingURL=category.controller.js.map
