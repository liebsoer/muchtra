'use strict';

var express = require('express');
var controller = require('./set.controller');

var router = express.Router();

router.get('/', controller.getSets);
router.put('/:set', controller.addOrUpdateSet);
router.head('/:set', controller.isHasSet);
router['delete']('/:set', controller.deleteSet);

module.exports = router;
//# sourceMappingURL=index.js.map
