'use strict';

angular.module('app2App')
  .directive('pageheader', function () {
    return {
      controller: 'HeaderCtrl',
      controllerAs: 'header',
      templateUrl: 'components/header/header.html',
      restrict: 'E',
      link: function (scope, element) {
        element.addClass('header');
        element.addClass('row');
      }
    };
  });
