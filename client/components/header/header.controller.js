'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var header;
        (function(header){
            var HeaderController = (function(){
                function HeaderController(){
                    this.logoSrc = 'assets/images/yeoman.png';
                    this.nameLong = 'Multiple Choice Training';
                    this.nameShort = 'MuChTra';
                }

                return HeaderController;
            })();

            header.HeaderController = HeaderController;
        })(header = muchtra.header || (muchtra.header = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App')
      .controller('HeaderCtrl', muchtra.header.HeaderController);
})();