'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var navbar;
        (function(navbar){
            var NavbarView = (function(){
                function NavbarView(){}
                
                NavbarView.bootstrap = function(){
                    
                };
                
                NavbarView.$inject = [
                    
                ];

                return NavbarView;
            })();

            navbar.HeaderController = NavbarView;
        })(navbar = muchtra.navbar || (muchtra.navbar = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App').run(['$rootScope', muchtra.navbar.HeaderController.bootstrap]);
})();