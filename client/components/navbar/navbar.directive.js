'use strict';

angular.module('app2App')
  .directive('navbar', function () {
    return {
      controller: 'NavbarController',
      controllerAs: 'navbar',
      templateUrl: 'components/navbar/navbar.html',
      restrict: 'E',
      link: function (scope, element) {
        element.addClass('navbar');
      }
    };
  });