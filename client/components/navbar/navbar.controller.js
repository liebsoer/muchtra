'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var navbar;
        (function(navbar){
            var NavbarController = (function(){
                function NavbarController($rootScope, $location, $http){
                    this.$location = $location;
                    this.$http = $http;
                    
                    var that = this;
                    that.activeRoute = '/home';
                    
                    $rootScope.$on( '$routeChangeSuccess', function(event,  current) {
                        that.activeRoute = current.originalPath;
                    });
                }
                
                NavbarController.prototype.isActiveMainRoute = function(route){
                    return this.activeRoute.indexOf(route) === 0;
                };
                
                NavbarController.prototype.isActiveSubroute = function(route){
                    return this.activeRoute.indexOf(route) === 0;
                };
                
                NavbarController.prototype.logout = function(){
                    var that = this;
                    this.$http.post('/api/auth/logout').then(function(){
                        that.$location.path('/login');
                    }, function(response){
                        console.error('Error during logout', response);
                        that.$location.path('/home');
                    });
                };
                
                NavbarController.$inject = [
                    '$rootScope',
                    '$location',
                    '$http'
                ];

                return NavbarController;
            })();

            navbar.NavbarController = NavbarController;
        })(navbar = muchtra.navbar || (muchtra.navbar = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App').controller('NavbarController', muchtra.navbar.NavbarController);
})();