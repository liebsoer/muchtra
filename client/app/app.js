'use strict';

angular.module('app2App', [
  'app2App.constants',
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'checklist-model'
]).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/home', {
        templateUrl: 'app/overview/overview.html',
        controller: 'OverviewCtrl'
    }).when('/preferences', {
        templateUrl: 'app/preferences/preferences.html',
        controller: 'PreferencesCtrl'
    }).otherwise({
        redirectTo: '/login'
    });

    $locationProvider.html5Mode(true);
}]);