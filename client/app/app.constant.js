(function(angular, undefined) {
'use strict';

angular.module('app2App.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);