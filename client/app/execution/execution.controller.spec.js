'use strict';

describe('Controller: ExecutionCtrl', function () {

  // load the controller's module
  beforeEach(module('app2App'));

  var ExecutionCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExecutionCtrl = $controller('ExecutionCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
