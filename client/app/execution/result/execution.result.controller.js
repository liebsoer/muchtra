'use strict';

(function () {
    var muchtra;
    (function (muchtra) {
        var app;
        (function (app) {
            var execution;
            (function () {
                var ExecutionResultController = (function () {
                    function ExecutionResultController($http, $location) {
                        this.$http = $http;
                        this.$location = $location;
                        
                        this.isOverview = typeof $location.search().id === 'undefined';
                        
                        this.overviewData = {};
                        this.resultData = {};
                        
                        if(this.isOverview){
                            this.injectOverviewData();
                        } else {
                            this.injectRunData();
                        }
                    }
                    
                    ExecutionResultController.prototype.injectOverviewData = function(){
                        var that = this;
                        this.$http.get('/api/execution/result').then(function(res){
                            that.overviewData = res.data.data;
                            
                            var keys = Object.keys(that.overviewData);
                            for(var i = 0; i < keys.length; i++){
                                if(typeof that.overviewData[keys[i]].answers === 'undefined' || that.overviewData[keys[i]].answers.length === 0){
                                    delete that.overviewData[keys[i]];
                                    keys.splice(i, 1);
                                    i--;
                                    continue;
                                }
                                
                                if(!that.overviewData[keys[i]].sets){
                                    that.overviewData[keys[i]].sets = [];
                                }
                                
                                if(!that.overviewData[keys[i]].state){
                                    that.overviewData[keys[i]].state = 'not-started';
                                }
                                
                                that.overviewData[keys[i]].meta = that.getQuestData(that.overviewData[keys[i]]);
                            }
                        });
                    };
                    
                    ExecutionResultController.prototype.injectRunData = function(){
                        var that = this;
                        this.$http.get('/api/execution/result/' + this.$location.search().id).then(function(res){
							
                            that.resultData = res.data.data;
							
							that.resultData.mappedAnswers = [];
							that.resultData.answers.forEach(function(item){
								that.resultData.mappedAnswers.push({
									question: item.question,
									answer: that.printAnswers(item, that.resultData.questions)
								});
							});
							
                            that.resultData.meta = that.getQuestData(that.resultData);
							
                        });
                    };
                    
                    ExecutionResultController.prototype.getQuestData = function(data){
                        var result = {
                            total: (data.answers ||{}).length || 0,
                            answered: 0,
                            correct: 0,
                            failed: 0
                        };
                        
                        for(var i = 0; i < data.answers.length; i++){
                            var answer = data.answers[i];
                            if(!answer.result || answer.state === 'init'){
                                continue;
                            }
                            
                            result.answered++;
                            
                            if(answer.result === 'success'){
                                result.correct++;
                            } else {
                                result.failed++;
                            }
                        }
                        
                        return result;
                    };
                    
                    ExecutionResultController.prototype.showResult = function(runId){
                        this.isOverview = false;
                        this.$location.search('id', runId);
                    };
					
					ExecutionResultController.prototype.isArray = function(obj){
						return angular.isArray(obj);
					}
					
					ExecutionResultController.prototype.printAnswers = function(question, allQuestions){
						var quest = question.question;
						var chosenAnswer = question.answer;
						
						
						
						var allAnswers = allQuestions[quest].answers;
						var correctAnswer = allQuestions[quest].correctAnswer;
						
						var res = [];
						
						if(angular.isArray(chosenAnswer)){
							for(var i = 0; i < allAnswers.length; i++){
								var obj = {
									answer: allAnswers[i]
								};
								var answer = allAnswers[i];
								
								if(chosenAnswer.indexOf(answer) >= 0 && correctAnswer.indexOf(answer) >= 0) {
									obj.status = 'success';
									obj.cls = 'execution-result-success';
								} else if (chosenAnswer.indexOf(answer) >= 0) {
									obj.status = 'fail';
									obj.cls = 'execution-result-fail';
								} else if (correctAnswer.indexOf(answer) >= 0) {
									obj.status = 'none';
									obj.cls = 'execution-result-real-right';
								} else {
									obj.status = 'none';
									obj.cls = 'execution-result-none';
								}
								
								res.push(obj);
							}
						} else {
							for(var i = 0; i < allAnswers.length; i++){
								var obj = {
									answer: allAnswers[i]
								};
								if(allAnswers[i] === chosenAnswer){
									obj.status = correctAnswer === chosenAnswer;
									obj.cls = correctAnswer === chosenAnswer ? 'execution-result-success' : 'execution-result-fail';
								} else if(correctAnswer.indexOf(allAnswers[i]) >= 0) {
									obj.status = 'none';
									obj.cls = 'execution-result-real-right';
								} else {
									obj.status = 'none';
									obj.cls = 'execution-result-none';
								}
								
								res.push(obj);
							}
							
						}
						
						return res;
					};

                    ExecutionResultController.$inject = [
                        '$http',
                        '$location'
                    ];

                    return ExecutionResultController;
                })();

                execution.ExecutionResultController = ExecutionResultController;

            })(execution = app.execution || (app.execution = {}));


        })(app = muchtra.app || (muchtra.app = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('ExecutionResultCtrl', muchtra.app.execution.ExecutionResultController);
})();