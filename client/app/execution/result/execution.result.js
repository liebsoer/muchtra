'use strict';

angular.module('app2App').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/execution/result', {
        templateUrl: 'app/execution/result/execution.result.html',
        controller: 'ExecutionResultCtrl',
        controllerAs: 'ctrl'
    });
}]);