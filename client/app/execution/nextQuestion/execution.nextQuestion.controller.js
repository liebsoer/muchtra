'use strict';

(function () {
    var muchtra;
    (function (muchtra) {
        var app;
        (function (app) {
            var execution;
            (function () {
                var ExecutionNextQuestionModalController = (function () {
                    function ExecutionNextQuestionModalController($uibModalInstance, $http, parentCtrl) {
                        this.$uibModalInstance = $uibModalInstance;
                        this.$http = $http;
                        this.parentCtrl = parentCtrl;
                        this.waitingForResponse = false;
                    }

                    ExecutionNextQuestionModalController.prototype.nextQuestion = function () {
                        var that = this;
                        this.parentCtrl.currentQuestion++;
                        this.parentCtrl.submitAnswers().then(function () {
                            that.parentCtrl.showResultsIfFinished();
                            that.$uibModalInstance.dismiss('cancel');
                        });
                    };

                    ExecutionNextQuestionModalController.$inject = [
                    '$uibModalInstance',
                    '$http',
                    'parentCtrl'
                ];

                    return ExecutionNextQuestionModalController;
                })();

                execution.ExecutionNextQuestionModalController = ExecutionNextQuestionModalController;

            })(execution = app.execution || (app.execution = {}));


        })(app = muchtra.app || (muchtra.app = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('ExecutionNextQuestionModalCtrl', muchtra.app.execution.ExecutionNextQuestionModalController);
})();