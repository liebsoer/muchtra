'use strict';

(function () {
    var muchtra;
    (function (muchtra) {
        var app;
        (function (app) {
            var execution;
            (function () {
                var ExecutionRunController = (function () {
                    function ExecutionRunController($location, $uibModal, $http) {
                        this.$location = $location;
                        this.$uibModal = $uibModal;
                        this.$http = $http;

                        this.runType = $location.search().runType || 'all';
                        this.sets = $location.search().sets || '';
                        this.execId = $location.search().id || null;

                        this.questions = [];

                        this.answers = [];
                        this.currentQuestion = 0;

                        this.errorText = '';
                        this.injectData();
                    }

                    ExecutionRunController.prototype.answerQuestion = function (index) {
                        var questionAnswer = this.answers[index];

                        if (questionAnswer.answer.length === 0) {
                            questionAnswer.state = 'error';
                            if (this.questions[index].type === 'radio') {
                                this.errorText = 'An answer has to be choosen!';
                            } else if (this.questions[index].type === 'checkbox') {
                                this.errorText = 'At least one answer has to be selected!';
                            }
                            return;
                        }

                        this.errorText = '';
                        questionAnswer.state = '';
                        questionAnswer.result = '';

                        var that = this;
                        var state = this.isValid(index);
                        
                        if (this.isValid(index)) {
                            questionAnswer.state = 'success';
                            questionAnswer.result = 'success';
                        } else {
                            questionAnswer.state = 'error';
                            questionAnswer.result = 'failed';
                            this.errorText = 'Answer is wrong';
                        }
                        
                        this.$uibModal.open({
                            animation: true,
                            controller: 'ExecutionNextQuestionModalCtrl',
                            controllerAs: 'ctrl',
                            templateUrl: 'app/execution/nextQuestion/execution.nextQuestion.html',
                            resolve: {
                                parentCtrl: function () {
                                    return that;
                                },
                                state: state
                            },
                            size: 'md'
                        });
                        
                        
                    };

                    ExecutionRunController.prototype.injectData = function () {
                        var that = this;
                        this.$http.get('/api/execution/question', {
                            params: {
                                'run-type': this.runType,
                                sets: this.sets
                            }
                        }).then(function (res) {
                            that.questions = res.data.data;
							
							var i, j, k, tmp;
							for(i = 0; i < that.questions.length; i++){
								j = Math.floor(Math.random() * (i + 1));
								tmp = that.questions[i];
								that.questions[i] = that.questions[j];
								that.questions[j] = tmp;
							}
//							for(i = 0; i < that.questions.length; i++){
//								for(j = 0; j < that.questions[i].answers.length; j++){
//									k = Math.floor(Math.random() * (j + 1));
//									tmp = that.questions[i].answers[j];
//									that.questions[i].answers[j] = that.questions[i].answers[k];
//									that.questions[i].answers[k] = tmp;
//								}
//							}
							
                            for (i = 0; i < that.questions.length; i++) {
                                that.answers[i] = {
                                    answer: [],
                                    question: that.questions[i].question,
                                    result: null,
                                    state: 'init'
                                };
                            }
                        });
                    };

                    ExecutionRunController.prototype.isValid = function (index) {
                        var question = this.questions[index];
                        var answer = this.answers[index];

                        if (angular.isArray(question.answers) && angular.isArray(answer.answer) && question.answers.length === answer.answer.length) {
                            for (var i = 0; i < answer.answer.length; i++) {
                                if (question.answers.indexOf(answer.answer[i]) === -1) {
                                    return false;
                                }
                            }
                            return true;
                        } else if (angular.isString(question.correctAnswer) && angular.isString(answer.answer) && question.correctAnswer === answer.answer) {
                            return true;
                        }

                        return false;
                    };

                    ExecutionRunController.prototype.showResultsIfFinished = function () {
                        if (this.questions.length - 1 < this.currentQuestion) {
                            this.$location.path('/execution/result').search({
                                id: this.execId
                            });
                        }
                    };

                    ExecutionRunController.prototype.submitAnswers = function () {
                        return this.$http.put('/api/execution/' + this.execId, {
                            answers: this.answers,
                            currentQuestion: this.currentQuestion,
                            state: this.questions.length - 1 > this.currentQuestion ? 'running' : 'finished'
                        });
                    };

                    ExecutionRunController.$inject = [
                    '$location',
                    '$uibModal',
                    '$http'
                ];

                    return ExecutionRunController;
                })();

                execution.ExecutionRunController = ExecutionRunController;

            })(execution = app.execution || (app.execution = {}));


        })(app = muchtra.app || (muchtra.app = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('ExecutionRunCtrl', muchtra.app.execution.ExecutionRunController);
})();