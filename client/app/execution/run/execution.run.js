'use strict';

angular.module('app2App').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/execution/run', {
        templateUrl: 'app/execution/run/execution.run.html',
        controller: 'ExecutionRunCtrl',
        controllerAs: 'ctrl'
    });
}]);