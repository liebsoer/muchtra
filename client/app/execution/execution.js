'use strict';

angular.module('app2App').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/execution', {
        templateUrl: 'app/execution/execution.html',
        controller: 'ExecutionCtrl',
        controllerAs: 'ctrl'
    });
}]);