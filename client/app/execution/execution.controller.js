'use strict';

(function () {
    var muchtra;
    (function (muchtra) {
        var app;
        (function (app) {
            var ExecutionController = (function () {
                function ExecutionController($location, $uibModal, $http) {
                    this.$location = $location;
                    this.$uibModal = $uibModal;
                    this.$http = $http;

                    this.runTypes = {
                        'all': 'All',
                        'failed': 'Failed',
                        'not-answered': 'Not answered',
                        'not-answered-failed': 'Not answered or failed'
                    };
                    this.selectedRunType = 'all';

                    this.defaultCategory = {
                        name: 'All categories',
                        description: 'All sets are displayed without restirction'
                    };
                    this.selectedCategory = this.defaultCategory;
                    this.categories = [this.selectedCategory];
                    this.setFilter = '';
                    this.entries = '';
                    this.selectedSets = [];

                    this.injectData();
                }

                ExecutionController.prototype.injectData = function () {
                    var that = this;
                    this.$http.get('/api/execution').then(function (res) {
                        var data = res.data.data;
                        if (data.categories) {
                            that.categories = [{
                                name: 'All categories',
                                description: 'All sets are displayed without restirction'
                            }].concat(data.categories);
                        }
                        if (data.sets) {
                            that.entries = data.sets;
                        }
                        if (data.statistics) {
                            that.statistics = data.statistics;
                        }
                    });
                };

                ExecutionController.prototype.execute = function () {
                    var that = this;
                    this.$http.post('/api/execution', {
                        runType: this.selectedRunType,
                        sets: this.selectedSets
                    }).then(function (res) {
                        that.$location.path('/execution/run').search({
                            type: that.selectedRunType,
                            sets: that.selectedSets,
                            id: res.data.data
                        });
                    });
                };

                ExecutionController.prototype.reset = function () {
                    this.selectedCategory = this.defaultCategory;
                    this.selectedRunType = 'all';
                    this.selectedSets = [];
                    this.setFilter = '';
                };

                ExecutionController.prototype.selectCategory = function (category) {
                    console.log(category);
                    this.selectedCategory = category;
                };

                ExecutionController.$inject = [
                    '$location',
                    '$uibModal',
                    '$http'
                ];

                return ExecutionController;
            })();

            app.ExecutionController = ExecutionController;
        })(app = muchtra.app || (muchtra.app = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('ExecutionCtrl', muchtra.app.ExecutionController);
})();