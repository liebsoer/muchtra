'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var setup;
        (function(setup){
            var QuestionController = (function(){
                function QuestionController($location, $uibModal, $http) {
                    this.$location = $location;
                    this.$uibModal = $uibModal;
                    this.$http = $http;
                    
                    this.questions = [];
                    this.sets = [];
                    
                    this.injectQuestions();
                    this.injectSets();
                }
                
                QuestionController.prototype.callAddQuestionModal = function () {
                    var that = this;
                    this.$uibModal.open({
                        animation: true,
                        controller: 'AddQuestionCtrl',
                        controllerAs: 'ctrl',
                        templateUrl: 'app/setup/add-question/add-question.html',
                        resolve: {
                            parentCtrl: function () {
                                return that;
                            }
                        },
                        size: 'lg'
                    });
                };

                QuestionController.prototype.editQuestion = function (question) {
                    var that = this;
                    this.$uibModal.open({
                        animation: true,
                        controller: 'EditQuestionCtrl',
                        controllerAs: 'ctrl',
                        templateUrl: 'app/setup/edit-question/edit-question.html',
                        resolve: {
                            question: function () {
                                return question;
                            },
                            parentCtrl: function () {
                                return that;
                            }
                        },
                        size: 'lg'
                    });
                };

                QuestionController.prototype.deleteQuestion = function (question) {
                    console.log('Delete question ' + question);
                    var that = this;
                    that.$http.delete('/api/question/' + question).then(function () {
                        for (var i = 0; i < that.questions.length; i++) {
                            if (that.questions[i].name === question) {
                                that.question.splice(i, 1);
                            }
                        }
                        that.injectQuestions();
                    });
                };
                
                QuestionController.prototype.injectSets = function(){
                    var that = this;
                    that.$http.get('/api/set').then(function(res){
                        that.sets = res.data.data;
                    }, function(){
                        that.sets = [{
                            description: 'Default Set',
                            name: 'Default'
                        }];
                    });
                };

                QuestionController.prototype.injectQuestions = function () {
                    var that = this;
                    that.$http.get('/api/question').then(function (res) {
                        that.questions = res.data.data.map(function(obj){
                            
                            obj.answers = obj.answers.map(function(answer){
                                return answer.replace(/(\\\\?n|\n)/g, '<br>');
                            });
                            if(obj.correctAnswer instanceof Array){
                                obj.correctAnswer = obj.correctAnswer.map(function(correctAnswer){
                                    return correctAnswer.replace(/(\\\\?n|\n)/g, '<br>');
                                });
                            } else {
                                obj.correctAnswer = obj.correctAnswer.replace(/(\\\\?n|\n)/g, '<br>');
                            }
                            obj.question = obj.question.replace(/(\\\\?n|\n)/g, '<br>');
                            
                            return obj;
                        });
                    });
                };
                
                QuestionController.$inject = [
                    '$location',
                    '$uibModal',
                    '$http'
                ];

                return QuestionController;
            })();

            setup.QuestionController = QuestionController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App').controller('QuestionCtrl', muchtra.setup.QuestionController);
})();