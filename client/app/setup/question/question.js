'use strict';

angular.module('app2App').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/setup/question', {
        templateUrl: 'app/setup/question/question.html',
        controller: 'QuestionCtrl'
    });
}]);