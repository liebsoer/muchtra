'use strict';

angular.module('app2App').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/setup/set', {
        templateUrl: 'app/setup/set/set.html',
        controller: 'SetCtrl'
    });
}]);