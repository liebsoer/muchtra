'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var setup;
        (function(setup){
            var SetController = (function(){
                function SetController($location, $uibModal, $http) {
                    this.$location = $location;
                    this.$uibModal = $uibModal;
                    this.$http = $http;
                    
                    this.sets = [];
                    this.categories = [];
                    
                    this.injectSets();
                    this.injectCategories();
                }
                
                SetController.prototype.callAddSetModal = function () {
                    var that = this;
                    this.$uibModal.open({
                        animation: true,
                        controller: 'AddSetCtrl',
                        controllerAs: 'ctrl',
                        templateUrl: 'app/setup/add-set/add-set.html',
                        resolve: {
                            parentCtrl: function () {
                                return that;
                            }
                        },
                        size: 'lg'
                    });
                };

                SetController.prototype.editSet = function (name, description, categories) {
                    var that = this;
                    this.$uibModal.open({
                        animation: true,
                        controller: 'EditSetCtrl',
                        controllerAs: 'ctrl',
                        templateUrl: 'app/setup/edit-set/edit-set.html',
                        resolve: {
                            description: function () {
                                return description;
                            },
                            name: function () {
                                return name;
                            },
                            categories: function(){
                                return categories;
                            },
                            parentCtrl: function () {
                                return that;
                            }
                        },
                        size: 'lg'
                    });
                };

                SetController.prototype.deleteSet = function (set) {
                    var that = this;
                    that.$http.delete('/api/set/' + set).then(function () {
                        console.log('Set deleted' + set);
                        for (var i = 0; i < that.sets.length; i++) {
                            if (that.sets[i].name === set) {
                                that.sets.splice(i, 1);
                            }
                        }
                        that.injectSets();
                    });
                };
                
                SetController.prototype.injectCategories = function(){
                    var that = this;
                    that.$http.get('/api/category').then(function(res){
                        that.categories = res.data.data;
                    }, function(){
                        that.categories = [{
                            description: 'Default Category',
                            name: 'Default'
                        }];
                    });
                };

                SetController.prototype.injectSets = function () {
                    var that = this;
                    that.$http.get('/api/set').then(function (res) {
                        that.sets = res.data.data;
                    });
                };
                
                SetController.$inject = [
                    '$location',
                    '$uibModal',
                    '$http'
                ];

                return SetController;
            })();

            setup.SetController = SetController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App').controller('SetCtrl', muchtra.setup.SetController);
})();