'use strict';

describe('Controller: SetCtrl', function () {

  // load the controller's module
  beforeEach(module('app2App'));

  var SetCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SetCtrl = $controller('SetCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
