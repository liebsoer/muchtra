'use strict';

describe('Controller: AddSetCtrl', function () {

  // load the controller's module
  beforeEach(module('app2App'));

  var AddSetCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddSetCtrl = $controller('AddSetCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
