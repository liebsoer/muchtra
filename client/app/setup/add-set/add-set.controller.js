'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var setup;
        (function(setup){
            var AddSetController = (function(){
                function AddSetController($uibModalInstance, $http, parentCtrl){
                    this.$uibModalInstance = $uibModalInstance;
                    this.$http = $http;
                    this.parentCtrl = parentCtrl;
                    this.setName  = '';
                    this.setDescription  = '';
                    this.errorMessages = [];
                    
                    this.selectedCategories = [];
                    this.unselectedCategories = angular.copy(this.parentCtrl.categories);
                }
                
                AddSetController.prototype.closeModal = function(){
                    this.$uibModalInstance.dismiss('cancel');
                };
                
                AddSetController.prototype.addCategory = function(category){
                    this.selectedCategories.push(category);
                    this.unselectedCategories.splice(this.unselectedCategories.indexOf(category), 1);
                };
                
                AddSetController.prototype.addSet = function(){
                    
                    if(!this.validate()){
                        return;
                    }
                    
                    var name = this.setName;
                    var description = this.setDescription;
                    
                    var that = this;
                    
                    this.$http.head('/api/set/' + name).then(function(){
                        that.$http.put('/api/set/' + name, {
                            data: {
                                categories: that.selectedCategories,
                                description: description
                            },
                            cmd: 'add'
                        }).then(function(){
                            console.info('Set ' + name + ' created.');
                            that.parentCtrl.injectSets();
                        }, function(res){
                            console.error('Error during creation of ' + name + '!', res);
                        });
                    }, function(res){
                        console.error('Set '  + name + ' already exists.', res);
                    });
                    
                    this.closeModal();
                };
                
                AddSetController.prototype.removeCategory = function(category){
                    console.log(category);
                    if(this.selectedCategories.indexOf(category) !== -1){
                        this.selectedCategories.splice(this.selectedCategories.indexOf(category), 1);
                        this.unselectedCategories.push(category);
                        this.unselectedCategories.sort();
                        this.selectedCategories.sort();
                    }
                };
                
                AddSetController.prototype.validate = function(){
                    this.errorMessages = [];
                    if(!this.setName || this.setName.length === 0){
                        this.errorMessages.push('Set name is mandator!');
                    }
                    if(this.selectedCategories.length === 0){
                        this.errorMessages.push('At least one category has to be selected!');
                    }
                    
                    if(this.errorMessages && this.errorMessages.length === 0){
                        this.errorMessages = [];
                        return true;
                    }
                    
                    return false;
                };
                
                AddSetController.$inject = [
                    '$uibModalInstance',
                    '$http',
                    'parentCtrl'
                ];

                return AddSetController;
            })();

            setup.AddSetController = AddSetController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App')
      .controller('AddSetCtrl', muchtra.setup.AddSetController);
})();