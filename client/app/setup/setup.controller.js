'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var header;
        (function(header){
            var SetupController = (function(){
                function SetupController(){
                
                }
                
                return SetupController;
            })();
            header.SetupController = SetupController;
        })(header = muchtra.header || (muchtra.header = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App')
      .controller('SetupCtrl', muchtra.header.SetupController);
})();