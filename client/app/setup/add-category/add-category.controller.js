'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var setup;
        (function(setup){
            var AddCategoryController = (function(){
                function AddCategoryController($uibModalInstance, $http, parentCtrl){
                    this.$uibModalInstance = $uibModalInstance;
                    this.$http = $http;
                    this.parentCtrl = parentCtrl;
                    this.categoryName  = '';
                    this.categoryDescription  = '';
                }
                
                AddCategoryController.prototype.closeModal = function(){
                    this.$uibModalInstance.dismiss('cancel');
                };
                
                AddCategoryController.prototype.addCategory = function(){
                    var name = this.categoryName;
                    var description = this.categoryDescription;
                    
                    var that = this;
                    
                    this.$http.head('/api/category/' + name).then(function(){
                        that.$http.put('/api/category/' + name, {
                            data: {
                                description: description
                            },
                            cmd: 'add'
                        }).then(function(){
                            console.info('Category ' + name + ' created.');
                            that.parentCtrl.injectCategories();
                        }, function(res){
                            console.error('Error during creation of ' + name + '!', res);
                        });
                    }, function(res){
                        console.error('Category '  + name + ' already exists.', res);
                    });
                    
                    this.closeModal();
                };
                
                AddCategoryController.$inject = [
                    '$uibModalInstance',
                    '$http',
                    'parentCtrl'
                ];

                return AddCategoryController;
            })();

            setup.AddCategoryController = AddCategoryController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App')
      .controller('AddCategoryCtrl', muchtra.setup.AddCategoryController);
})();