'use strict';

describe('Controller: AddQuestionCtrl', function () {

  // load the controller's module
  beforeEach(module('app2App'));

  var AddQuestionCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddQuestionCtrl = $controller('AddQuestionCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
