'use strict';
(function () {
    var muchtra;
    (function (muchtra) {
        var setup;
        (function (setup) {
            var AddQuestionController = (function () {
                function AddQuestionController($uibModalInstance, $http, parentCtrl) {
                    this.$uibModalInstance = $uibModalInstance;
                    this.$http = $http;
                    this.parentCtrl = parentCtrl;
                    this.question = '';
                    this.questionType = 'radio';
                    this.questionCorrectAnswer = '';
                    this.questionAnswers = [];
                    this.selectedSets = [];
                    this.unselectedSets = (function (sets) {
                        var result = [];
                        for (var i = 0; i < sets.length; i++) {
                            result.push({
                                name: sets[i].name,
                                description: sets[i].description
                            });
                        }
                        return result;
                    })(angular.copy(this.parentCtrl.sets));
                    this.newAnswer = '';
                    this.errorMessages = '';
                }

                AddQuestionController.prototype.closeModal = function () {
                    this.$uibModalInstance.dismiss('cancel');
                };

                AddQuestionController.prototype.addAnswer = function () {
                    if (this.questionAnswers.indexOf(this.newAnswer) === -1) {
                        this.questionAnswers.push(this.newAnswer);
                    }
                    this.newAnswer = '';
                };

                AddQuestionController.prototype.addSet = function (set) {
                    this.selectedSets.push(set);
                    for (var i = 0; i < this.unselectedSets.length; i++) {
                        if (this.unselectedSets[i].name === set.name) {
                            this.unselectedSets.splice(i, 1);
                            break;
                        }
                    }
                };

                AddQuestionController.prototype.addQuestion = function () {

                    if (!this.validate()) {
                        return;
                    }

                    var question = this.question;
                    var type = this.questionType;
                    var answers = this.questionAnswers;
                    var correctAnswer = this.questionCorrectAnswer;
                    var sets = this.selectedSets;

                    var that = this;

                    this.$http.head('/api/question/' + encodeURIComponent(question)).then(function () {
                        that.$http.put('/api/question/' + encodeURIComponent(question), {
                            data: {
                                type: type,
                                answers: answers,
                                correctAnswer: correctAnswer,
                                sets: sets,
                                question: question
                            },
                            cmd: 'add'
                        }).then(function () {
                            console.info('Question \"' + question + '\" created.');
                            that.parentCtrl.injectQuestions();
                        }, function (res) {
                            console.error('Error during creation of \"' + name + '\"!', res);
                        });
                    }, function (res) {
                        console.error('Question ' + question + ' already exists.', res);
                    });

                    this.closeModal();
                };

                AddQuestionController.prototype.onTypeChange = function () {
                    if (this.questionType === 'radio') {
                        this.questionCorrectAnswer = '';
                    } else {
                        this.questionCorrectAnswer = [];
                    }
                };

                AddQuestionController.prototype.removeSet = function (set) {
                    for (var i = 0; i < this.selectedSets.length; i++) {
                        if (this.selectedSets[i].name === set.name) {
                            this.selectedSets.splice(i, 1);
                            i--;
                            this.unselectedSets.push(set);
                        }
                        break;
                    }
                };

                AddQuestionController.prototype.validate = function () {
                    this.errorMessages = [];

                    if (this.question.trim().length === 0) {
                        this.errorMessages.push('Question is mandatory!');
                    }
                    if (this.questionAnswers.length < 2) {
                        this.errorMessages.push('At least two possible answers have to be available!');
                    }
                    if (this.questionType === 'radio' && this.questionCorrectAnswer.trim().length === 0 || this.questionType === 'checkbox' && this.questionCorrectAnswer.length < 1) {
                        this.errorMessages.push('At least one correct answer has to be selected!');
                    }
                    if (this.selectedSets.length === 0) {
                        this.errorMessages.push('At least one set has to be selected!');
                    }
                    if (this.errorMessages.length === 0) {
                        return true;
                    }

                    return false;
                };

                AddQuestionController.$inject = [
                    '$uibModalInstance',
                    '$http',
                    'parentCtrl'
                ];

                return AddQuestionController;
            })();

            setup.AddQuestionController = AddQuestionController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('AddQuestionCtrl', muchtra.setup.AddQuestionController);
})();