'use strict';
(function () {
    var muchtra;
    (function (muchtra) {
        var setup;
        (function (setup) {
            var EditSetController = (function () {
                function EditSetController($uibModalInstance, $http, parentCtrl, name, description, categories) {
                    this.$uibModalInstance = $uibModalInstance;
                    this.$http = $http;
                    this.parentCtrl = parentCtrl;
                    this.setName = name || '';
                    this.setOriginalName = name || '';
                    this.setDescription = description || '';
                    this.errorMessages = [];


                    this.selectedCategories = categories || [];
                    
                    this.unselectedCategories = (function () {
                        var allCategories = angular.copy(parentCtrl.categories);
                        if (categories && categories.length > 0) {
                            for (var i = 0; i < allCategories.length; i++) {
                                for (var j = 0; j < categories.length; j++) {
                                    if(allCategories[i].name === categories[j].name){
                                        allCategories.splice(i, 1);
                                        i--;
                                        break;
                                    }
                                }
                            }
                        }
                        return allCategories;
                    })();
                }

                EditSetController.prototype.addCategory = function (category) {
                    this.selectedCategories.push(category);
                    this.unselectedCategories.splice(this.unselectedCategories.indexOf(category), 1);
                };

                EditSetController.prototype.closeModal = function () {
                    this.$uibModalInstance.dismiss('cancel');
                };

                EditSetController.prototype.updateSet = function () {
                    if(!this.validate()){
                        return;
                    }
                    
                    var name = this.setName;
                    var description = this.setDescription;
                    var categories = this.selectedCategories;
                    var that = this;

                    this.$http.put('/api/set/' + name, {
                        data: {
                            categories: categories,
                            description: description,
                            oldName: that.setOriginalName
                        },
                        cmd: 'update'
                    }).then(function () {
                        console.info('Set ' + name + ' updated.');
                        that.parentCtrl.injectSets();
                    }, function (res) {
                        console.error('Error during creation of ' + name + '!', res);
                    });

                    this.closeModal();
                };
                
                EditSetController.prototype.removeCategory = function(category){
                    console.log(category);
                    if(this.selectedCategories.indexOf(category) !== -1){
                        this.selectedCategories.splice(this.selectedCategories.indexOf(category), 1);
                        this.unselectedCategories.push(category);
                        this.unselectedCategories.sort();
                        this.selectedCategories.sort();
                    }
                };
                
                EditSetController.prototype.validate = function(){
                    this.errorMessages = [];
                    if(!this.setName || this.setName.length === 0){
                        this.errorMessages.push('Set name is mandator!');
                    }
                    if(this.selectedCategories.length === 0){
                        this.errorMessages.push('At least one category has to be selected!');
                    }
                    
                    if(this.errorMessages && this.errorMessages.length === 0){
                        this.errorMessages = [];
                        return true;
                    }
                    
                    return false;
                };

                EditSetController.$inject = [
                    '$uibModalInstance',
                    '$http',
                    'parentCtrl',
                    'name',
                    'description',
                    'categories'
                ];

                return EditSetController;
            })();

            setup.EditSetController = EditSetController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('EditSetCtrl', muchtra.setup.EditSetController);
})();