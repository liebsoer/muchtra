'use strict';

describe('Controller: EditSetCtrl', function () {

  // load the controller's module
  beforeEach(module('app2App'));

  var EditSetCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditSetCtrl = $controller('EditSetCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
