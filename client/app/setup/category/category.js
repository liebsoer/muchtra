'use strict';

angular.module('app2App').config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/setup/category', {
        templateUrl: 'app/setup/category/category.html',
        controller: 'CategoryCtrl'
    });
}]);