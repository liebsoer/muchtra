'use strict';

(function () {
    var muchtra;
    (function (muchtra) {
        var header;
        (function (header) {
            var CategoryController = (function () {
                function CategoryController($location, $uibModal, $http) {
                    this.$location = $location;
                    this.$uibModal = $uibModal;
                    this.$http = $http;
                    this.categories = [];

                    this.injectCategories();
                }

                CategoryController.prototype.callAddCategoryModal = function () {
                    var that = this;
                    this.$uibModal.open({
                        animation: true,
                        controller: 'AddCategoryCtrl',
                        controllerAs: 'ctrl',
                        templateUrl: 'app/setup/add-category/add-category.html',
                        resolve: {
                            parentCtrl: function () {
                                return that;
                            }
                        },
                        size: 'lg'
                    });
                };

                CategoryController.prototype.editCategory = function (name, description) {
                    var that = this;
                    this.$uibModal.open({
                        animation: true,
                        controller: 'EditCategoryCtrl',
                        controllerAs: 'ctrl',
                        templateUrl: 'app/setup/edit-category/edit-category.html',
                        resolve: {
                            description: function () {
                                return description;
                            },
                            name: function () {
                                return name;
                            },
                            parentCtrl: function () {
                                return that;
                            }
                        },
                        size: 'lg'
                    });
                };

                CategoryController.prototype.deleteCategory = function (category) {
                    var that = this;
                    that.$http.delete('/api/category/' + category).then(function () {
                        for (var i = 0; i < that.categories.length; i++) {
                            if (that.categories[i].name === category) {
                                that.categories.splice(i, 1);
                            }
                        }
                    });
                };

                CategoryController.prototype.injectCategories = function () {
                    var that = this;
                    that.$http.get('/api/category').then(function (res) {
                        that.categories = res.data.data;
                    });
                };

                CategoryController.$inject = [
                    '$location',
                    '$uibModal',
                    '$http'
                ];

                return CategoryController;
            })();

            header.CategoryController = CategoryController;
        })(header = muchtra.header || (muchtra.header = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App')
        .controller('CategoryCtrl', muchtra.header.CategoryController);
})();