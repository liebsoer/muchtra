'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var setup;
        (function(setup){
            var EditCategoryController = (function(){
                function EditCategoryController($uibModalInstance, $http, parentCtrl, name, description){
                    this.$uibModalInstance = $uibModalInstance;
                    this.$http = $http;
                    this.parentCtrl = parentCtrl;
                    this.categoryName  = name || '';
                    this.categoryOriginalName  = name || '';
                    this.categoryDescription  = description || '';
                }
                
                EditCategoryController.prototype.closeModal = function(){
                    this.$uibModalInstance.dismiss('cancel');
                };
                
                EditCategoryController.prototype.updateCategory = function(){
                    var name = this.categoryName;
                    var description = this.categoryDescription;
                    
                    var that = this;
                    
                    this.$http.put('/api/category/' + name, {
                        data: {
                            description: description,
                            oldName: that.categoryOriginalName
                        },
                        cmd: 'update'
                    }).then(function(){
                        console.info('Category ' + name + ' updated.');
                        that.parentCtrl.injectCategories();
                    }, function(res){
                        console.error('Error during creation of ' + name + '!', res);
                    });
                    
                    this.closeModal();
                };
                
                EditCategoryController.$inject = [
                    '$uibModalInstance',
                    '$http',
                    'parentCtrl',
                    'name',
                    'description'
                ];

                return EditCategoryController;
            })();

            setup.EditCategoryController = EditCategoryController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App').controller('EditCategoryCtrl', muchtra.setup.EditCategoryController);
})();