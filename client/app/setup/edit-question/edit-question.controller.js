'use strict';
(function(){
    var muchtra;
    (function(muchtra) {
        var setup;
        (function(setup){
            var EditQuestionController = (function(){
                function EditQuestionController($uibModalInstance, $http, parentCtrl, question) {
                    question = angular.copy(question);
                    
                    this.$uibModalInstance = $uibModalInstance;
                    this.$http = $http;
                    this.parentCtrl = parentCtrl;
                    this.question = question.question || '';
                    this.questionType = question.type || 'radio';
                    this.questionCorrectAnswer = question.correctAnswer || '';
                    this.questionAnswers = question.answers || [];
                    this.selectedSets = question.sets || [];
                    
                    this.unselectedSets = (function(sets){
                        for(var i = 0; i < sets.length; i++){
                            for(var j = 0; j < question.sets.length; j++){
                                if(question.sets[j].name.indexOf(sets[i].name) !== -1){
                                    sets.splice(i, 1);
                                    i--;
                                    break;
                                }
                            }
                        }
                        return sets;
                    })(angular.copy(parentCtrl.sets));
                    this.newAnswer = '';
                    this.errorMessages = '';
                }

                EditQuestionController.prototype.closeModal = function () {
                    this.$uibModalInstance.dismiss('cancel');
                };

                EditQuestionController.prototype.addAnswer = function () {
                    if (this.questionAnswers.indexOf(this.newAnswer) === -1) {
                        this.questionAnswers.push(this.newAnswer);
                    }
                    this.newAnswer = '';
                };

                EditQuestionController.prototype.addSet = function (set) {
                    this.selectedSets.push(set);
                    for(var i = 0; i < this.unselectedSets.length; i++){
                        if(this.unselectedSets[i].name === set.name){
                            this.unselectedSets.splice(i, 1);
                            break;
                        }
                    }
                };

                EditQuestionController.prototype.addQuestion = function () {
                    
                    if(!this.validate()){
                        return;
                    }

                    var question = this.question;
                    var type = this.questionType;
                    var answers = this.questionAnswers;
                    var correctAnswer = this.questionCorrectAnswer;
                    var sets = this.selectedSets;
                    
                    var that = this;

                    this.$http.head('/api/question/' + question).then(function () {
                        that.$http.put('/api/question/' + question, {
                            data: {
                                type: type,
                                answers: answers,
                                correctAnswer:correctAnswer,
                                sets: sets,
                                question: question
                            },
                            cmd: 'add'
                        }).then(function () {
                            console.info('Question \"' + question + '\" created.');
                            that.parentCtrl.injectQuestions();
                        }, function (res) {
                            console.error('Error during creation of \"' + name + '\"!', res);
                        });
                    }, function (res) {
                        console.error('Question ' + question + ' already exists.', res);
                    });

                    this.closeModal();
                };

                EditQuestionController.prototype.onTypeChange = function () {
                    if (this.questionType === 'radio') {
                        this.questionCorrectAnswer = '';
                    } else {
                        this.questionCorrectAnswer = [];
                    }
                };
                
                EditQuestionController.prototype.removeSet = function(set){
                    for(var i = 0; i < this.selectedSets.length; i++){
                        if(this.selectedSets[i].name === set.name){
                            this.selectedSets.splice(i, 1);
                            this.unselectedSets.push(set);
                            break;
                        }
                    }
                };

                EditQuestionController.prototype.validate = function () {
                    this.errorMessages = [];

                    if (this.question.trim().length === 0) {
                        this.errorMessages.push('Question is mandatory!');
                    }
                    if (this.questionAnswers.length < 2) {
                        this.errorMessages.push('At least two possible answers have to be available!');
                    }
                    if (this.questionType === 'radio' && this.questionCorrectAnswer.trim().length === 0 || this.questionType === 'checkbox' && this.questionCorrectAnswer.length < 2) {
                        this.errorMessages.push('At least one correct answer has to be selected!');
                    }
                    if (this.selectedSets.length === 0){
                        this.errorMessages.push('At least one set has to be selected!');
                    }
                    if (this.errorMessages.length === 0) {
                        return true;
                    }

                    return false;
                };

                EditQuestionController.$inject = [
                    '$uibModalInstance',
                    '$http',
                    'parentCtrl',
                    'question'
                ];

                return EditQuestionController;
            })();

            setup.EditQuestionController = EditQuestionController;
        })(setup = muchtra.setup || (muchtra.setup = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));
    
    angular.module('app2App').controller('EditQuestionCtrl', muchtra.setup.EditQuestionController);
})();