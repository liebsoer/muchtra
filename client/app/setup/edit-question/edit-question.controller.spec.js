'use strict';

describe('Controller: EditQuestionCtrl', function () {

  // load the controller's module
  beforeEach(module('app2App'));

  var EditQuestionCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EditQuestionCtrl = $controller('EditQuestionCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
