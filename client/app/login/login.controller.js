'use strict';
(function () {
    var muchtra;
    (function (muchtra) {
        var login;
        (function (login) {
            var LoginController = (function () {
                function LoginController($location, $http, $uibModal) {
                    this.$location = $location;
                    this.$http = $http;
                    this.$uibModal = $uibModal;

                    this.username = '';
                    this.password = '';
                }

                LoginController.prototype.login = function () {
                    var that = this;
                    this.$http.post('/api/auth/login', {
                        username: this.username,
                        password: this.password
                    }).then(function (response) {
                        that.$uibModal.open({
                            animation: true,
                            controller: 'LoginAlertCtrl',
                            controllerAs: 'ctrl',
                            templateUrl: 'app/login/alert/alert.html',
                            resolve: {
                                title: function () {
                                    return response.status + ': ' + response.statusText;
                                },
                                text: function () {
                                    return (response.data || {}).message || '';
                                },
                                callback: function () {
                                    that.$location.path('/home');
                                }
                            },
                            size: 'md'
                        });
                    }, function (response) {
                        that.$uibModal.open({
                            animation: true,
                            controller: 'LoginAlertCtrl',
                            controllerAs: 'ctrl',
                            templateUrl: 'app/login/alert/alert.html',
                            resolve: {
                                title: function () {
                                    return response.status + ': ' + response.statusText;
                                },
                                text: function () {
                                    return (response.data || {}).message || '';
                                },
                                callback: function () {}
                            },
                            size: 'md'
                        });
                    });
                };

                LoginController.prototype.register = function () {
                    var that = this;
                    this.$http.post('/api/auth/register', {
                        username: this.username,
                        password: this.password
                    }).then(function (response) {
                        that.$uibModal.open({
                            animation: true,
                            controller: 'LoginAlertCtrl',
                            controllerAs: 'ctrl',
                            templateUrl: 'app/login/alert/alert.html',
                            resolve: {
                                title: function () {
                                    return 'Success';
                                },
                                text: function () {
                                    return (response.data || {}).message || '';
                                },
                                callback: function () {
                                    that.$location.path('/home');
                                }
                            },
                            size: 'md'
                        });
                    }, function (response) {
                        that.$uibModal.open({
                            animation: true,
                            controller: 'LoginAlertCtrl',
                            controllerAs: 'ctrl',
                            templateUrl: 'app/login/alert/alert.html',
                            resolve: {
                                title: function () {
                                    return response.status + ': ' + response.statusText;
                                },
                                text: function () {
                                    return (response.data || {}).message || '';
                                },
                                callback: function () {}
                            },
                            size: 'md'
                        });
                    });
                };

                LoginController.$inject = [
                    '$location',
                    '$http',
                    '$uibModal'
                ];

                return LoginController;
            })();

            login.LoginController = LoginController;
        })(login = muchtra.login || (muchtra.login = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('LoginCtrl', muchtra.login.LoginController);
})();