'use strict';

angular.module('app2App').config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'ctrl'
    });


    $httpProvider.interceptors.push(['$rootScope', '$q', '$timeout', '$location', function ($rootScope, $q, $timeout, $location) {

        return {
            response: function (response) {
                return response;
            },
            responseError: function (response) {
                console.log(response.status, $location.path(), $location.path().indexOf('/login'));
                if (response.status === 401 && $location.path().indexOf('/login') === -1) {
                    $location.path('/login');
                } else if (response.status === 403) {
                    $rootScope.$broadcast('InsufficientPrivileges');
                } 
                return $q.reject(response);
            }
        };
    }]);
}]);