'use strict';
(function () {
    var muchtra;
    (function (muchtra) {
        var login;
        (function (login) {
            var alert;
            (function (alert) {
                var AlertController = (function () {
                    function AlertController($uibModalInstance, title, text, callback) {
                        this.$uibModalInstance = $uibModalInstance;
                        this.title = title || '';
                        this.text = text || '';
                        this.callback = callback || function (){};
                    }

                    AlertController.prototype.close = function () {
                        this.$uibModalInstance.dismiss('cancel');
                        this.callback();
                    };

                    AlertController.$inject = [
                        '$uibModalInstance',
                        'title',
                        'text',
                        'callback'
                    ];

                    return AlertController;
                })();

                alert.AlertController = AlertController;
            })(alert = login.alert || (login.alert = {}));
        })(login = muchtra.login || (muchtra.login = {}));
    })(muchtra = window.muchtra || (window.muchtra = {}));

    angular.module('app2App').controller('LoginAlertCtrl', muchtra.login.alert.AlertController);
})();