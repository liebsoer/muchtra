'use strict';

module.exports = function (app) {
    var express = require('express');
    var controller = require('./login.controller');
    var router = express.Router();

    router.post('/login', controller.doPostLogin);
    router.post('/logout', controller.doPostLogout);
    router.post('/register', controller.doPostRegister);

    app.all('*', function (req, res, next) {
        if (req.url.startsWith('/api/auth') || ((req.url.startsWith('/login') || req.url.startsWith('/home')) && ['GET'].indexOf(req.method) !== -1)) {
            next();
        } else {
            controller.isAuthorized(req.cookies['muchtra-login']).then(function (loggedin) {
                if (loggedin) {
                    next();
                } else {
                    res.status(401).json({
                        data: null,
                        message: 'Not authorized',
                        status: 'error'
                    });
                }
            }, function (err) {
                res.status(500);
                res.json({
                    data: err,
                    message: err.message,
                    status: 'error'
                })
            });
        }
    });

    return router;
}