/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/set              ->  index
 */

'use strict';

var db = require('../../components/db');


exports.addOrUpdateSet = function (req, res) {
    db.set.getSets().then(function (sets) {
        var name = req.params.set;
        var oldName = req.body.data.oldName;
        var description = req.body.data.description;
        var categories = req.body.data.categories;
        if (name) {
            if (!oldName) {
                db.set.isHasSet(name).then(function (exists) {
                    if (!exists) {
                        sets.push({
                            name: name,
                            description: description || '',
                            categories: categories || []
                        });
                        db.set.writeSets(sets).then(function () {
                            res.status(200);
                            res.json({
                                data: null,
                                message: 'Set \"' + name + '\" created',
                                status: 'success'
                            });
                        }, function (err) {
                            res.status(500);
                            res.json({
                                data: err,
                                message: err.message,
                                status: 'error'
                            })
                        });
                    } else {
                        res.status(409);
                        res.json({
                            data: null,
                            message: 'Set \"' + name + '\" already existing!',
                            status: 'error'
                        });
                    }
                }, function (err) {
                    res.status(500);
                    res.json({
                        data: err,
                        message: err.message,
                        status: 'error'
                    });
                });
            } else {
                var message = '';
                for (var i = 0; i < sets.length; i++) {
                    if (name !== oldName && sets[i].name === oldName) {
                        sets.splice(i, 1);
                        sets.push({
                            name: name,
                            description: description || '',
                            categories: categories || []
                        });
                        message = 'Set \"' + oldName + ' renamed to \"' + name + '\"';
                        break;
                    } else if (sets[i].name === name) {
                        sets[i].name = name;
                        sets[i].description = description;
                        sets[i].categories = categories;
                        message = 'Set \"' + name + '\" updated';
                        break;
                    }
                }
                db.set.writeSets(sets).then(function () {
                    res.status(200);
                    res.json({
                        data: null,
                        message: message,
                        status: 'success'
                    });
                }, function (err) {
                    res.status(500);
                    res.json({
                        data: err,
                        message: err.message,
                        status: 'error'
                    })
                });
            }
        } else {
            res.status(412);
            res.json({
                data: null,
                message: 'No set name available',
                status: 'error'
            });
        }
    });
};

exports.deleteSet = function (req, res) {
    var set = req.params.set;

    if (!set) {
        res.status(412);
        res.json({
            data: null,
            message: 'No set name available',
            status: 'error'
        });
        return;
    }

    if (set.toLowerCase() === 'default') {
        res.status(405);
        res.json({
            data: null,
            message: 'Default set is not allowed to be deleted!',
            status: 'error'
        });
        return;
    }

    db.set.getSets().then(function (sets) {
        var success = function () {
            res.status(200);
            res.json({
                data: null,
                message: 'Set \"' + set + '\" succesfully deleted.',
                status: 'success'
            });
        };
        var failure = function (err) {
            res.status(500);
            res.json({
                data: err,
                message: err.message,
                status: 'error'
            });
        };

        for (var i = 0; i < sets.length; i++) {
            if (sets[i].name === set) {
                sets.splice(i, 1);
                db.set.writeSets(sets).then(success, failure);
                return;
            }
        }

        res.status(410);
        res.json({
            data: null,
            message: 'Set \"' + set + '\" was not found. Nothing was deleted.',
            status: 'error'
        });
    });

};

exports.getSets = function (req, res) {
    db.set.getSets().then(function (sets) {
        res.status(200);
        res.json({
            data: sets,
            message: null,
            satus: 'success'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });
};

exports.isHasSet = function (req, res) {
    db.set.isHasSet(req.params.set).then(function (exists) {
        res.status(200);
        res.json({
            data: exists,
            message: 'Set ' + (exists ? '' : 'does not ') + 'exists',
            status: 'success'
        });
    }, function (err) {
        res.status(500);
        res.json({
            data: err,
            message: err.message,
            status: 'error'
        });
    });

}