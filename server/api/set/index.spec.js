'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var setCtrlStub = {
  index: 'setCtrl.index'
};

var routerStub = {
  get: sinon.spy()
};

// require the index with our stubbed out modules
var setIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './set.controller': setCtrlStub
});

describe('Category API Router:', function() {

  it('should return an express router instance', function() {
    setIndex.should.equal(routerStub);
  });

  describe('GET /api/set', function() {

    it('should route to set.controller.index', function() {
      routerStub.get
        .withArgs('/', 'setCtrl.index')
        .should.have.been.calledOnce;
    });

  });

});
