'use strict';

var express = require('express');
var controller = require('./category.controller');

var router = express.Router();

router.get('/', controller.getCategories);
router.put('/:category', controller.addOrUpdateCategory);
router.head('/:category', controller.isHasCategory);
router.delete('/:category', controller.deleteCategory);

module.exports = router;
