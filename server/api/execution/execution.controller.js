'use strict';
module.exports = (function () {
    var db = require('../../components/db');
    var Q = require('Q');
    var crypto = require('crypto');
    var base64url = require('base64url');

    function initTestRunImpl(req, res) {

        var user = req.cookies['muchtra-user'];
        var runTyoe = req.body.runType;
        var sets = req.body.sets;

       db.results.getResults(user).then(function (data) {
            var execId = base64url(crypto.randomBytes(10));
            while (data[execId]) {
                execId = base64url(crypto.randomBytes(10));
            }

            data[execId] = {
                date: new Date().getTime(),
                runType: runTyoe,
                sets: sets
            };
            db.results.writeResults(user, data).then(function () {
                res.status(200);
                res.json({
                    data: execId,
                    message: null,
                    status: 'success'
                });
            }, function (err) {
                res.status(500);
                res.json({
                    data: err,
                    message: null,
                    status: 'error'
                });
            });

        }, function (err) {
            res.status(500);
            res.json({
                data: err,
                message: null,
                status: 'error'
            });
        });
    }
    
    function getOverviewDataImpl(req, res) {
        var user = req.cookies['muchtra-user'];
        db.results.getResults(user).then(function(data){
            res.status(200);
            res.json({
                data: data,
                message: null,
                status: 'success'
            });
        }, function (err) {
            res.status(500);
            res.json({
                data: err,
                message: null,
                status: 'error'
            });
        });
    }

    function getExecutionDataImpl(req, res) {
        var promises = [db.category.getCategories(), db.set.getSets(), db.question.getQuestions()];
        Q.all(promises).then(function (values) {
            var categories = values[0];
            var sets = values[1];
            var questions = values[2];

            var statistics = {};

            for (var i = 0; i < questions.length; i++) {
                if (questions[i].sets) {
                    for (var j = 0; j < questions[i].sets.length; j++) {
                        if (!statistics[questions[i].sets[j].name]) {
                            statistics[questions[i].sets[j].name] = 0;
                        }
                        statistics[questions[i].sets[j].name]++
                    }
                }
            }

            for (var i = 0; i < sets.length; i++) {
                if (statistics[sets[i].name] && statistics[sets[i].name] > 0) {
                    sets[i].statistics = {
                        amount: statistics[sets[i].name],
                        answered: 0,
                        failed: 0,
                        notAnswered: 0
                    };
                } else {
                    sets.splice(i, 1);
                    i--;
                }
            }

            var data = {
                categories: categories,
                sets: sets
            };

            res.status(200);
            res.json({
                data: data,
                message: null,
                status: 'success'
            });
        });
    }

    function getQuestionDataImpl(req, res) {

        var runType = req.query['run-type'] || 'all';
        var sets = req.query.sets || [];
        var user = req.cookies['muchtra-user'];


        Q.all([db.question.getQuestions(), db.results.getResults(user)]).then(function (values) {
            var questions = values[0];
            var results = values[1];

            for (var i = 0; i < questions.length; i++) {
                var inUse = false;
                for (var j = 0; j < sets.length; j++) {
                    if (questions[i].sets.indexOf(sets[j])) {
                        inUse = true;
                        break;
                    }
                }
                if (!inUse) {
                    questions.splice(i, 1);
                    i--;
                    continue;
                }

                switch (runType) {
                case 'failed':
                    if (results[questions[i]] && results[questions[i]].lastStatus !== 'failed') {
                        questions.splice(i, 1);
                        i--;
                    }
                    break;
                case 'not-answered':
                    if (!results[questions[i]] || results[questions[i]].lastStatus) {
                        questions.splice(i, 1);
                        i--;
                    }
                    break;
                case 'not-answered-failed':
                    if (!results[questions[i]] || results[questions[i]].lastStatus !== 'failed') {
                        questions.splice(i, 1);
                        i--;
                    }
                    break;
                default:
                    break;
                }

                if (questions[i].sets) {
                    delete questions[i].sets;
                }
            }
            res.status(200);
            res.json({
                data: questions,
                message: null,
                status: 'success'
            });
        });
    }
    
    function getResultDataImpl(req, res){
        var user = req.cookies['muchtra-user'];
        var id = req.params.id;
        
		Q.all([db.results.getResults(user), db.question.getQuestions()]).then(function (values) {
			var data = values[0];
			var sets = data[id].sets;
			var questions = values[1];
		    data[id].questions = {};
			
			
			try {
				for(var i = 0; i < questions.length; i++){
					var quest = questions[i];

					if(data[id].questions[quest.question]){
						break;
					}
					
					for(var j = 0; quest.sets && j <  quest.sets.length; j++){
						var set = quest.sets[j].name;
						if(sets.indexOf(set.name)){
							data[id].questions[quest.question] = quest;
							break;
						}
					}
				}
			} catch(e){
				console.error(e);
			}
			
			if(data[id]){
                res.status(200);
                res.json({
                    data: data[id],
                    message: null,
                    status: 'success'
                })
            } else {
                res.status(404);
                res.json({
                    data: null,
                    message: 'Result with run id ' + id + ' could not be found',
                    status: 'error'
                })
            }
			
		});
		
//        db.results.getResults(user).then(function(data){
//            if(data[id]){
//                res.status(200);
//                res.json({
//                    data: data[id],
//                    message: null,
//                    status: 'success'
//                })
//            } else {
//                res.status(404);
//                res.json({
//                    data: null,
//                    message: 'Result with run id ' + id + ' could not be found',
//                    status: 'error'
//                })
//            }
//        });
    }

    function persistTestRunImpl(req, res) {
        var user = req.cookies['muchtra-user'];
        var id = req.params.id;
        var reqData = req.body;

        db.results.getResults(user).then(function (data) {
            data[id].answers = reqData.answers;
            data[id].currentQuestion = reqData.currentQuestion;
            data[id].state = reqData.state;
            
            db.results.writeResults(user, data).then(function () {
                res.status(200);
                res.json({
                    data: data,
                    message: 'Answers updated',
                    status: 'success'
                });
            });
        }, function (err) {
            res.status(500);
            res.json({
                data: err,
                message: null,
                status: 'error'
            });
        });


    }

    return {
        initTestRun: function (req, res) {
            initTestRunImpl(req, res);
        },
        getExecutionData: function (req, res) {
            getExecutionDataImpl(req, res);
        },
        getOverviewData: function(req, res){
            getOverviewDataImpl(req, res);
        },
        getQuestionData: function (req, res) {
            getQuestionDataImpl(req, res);
        },
        getResultData: function(req, res){
            getResultDataImpl(req, res);
        },
        persistTestRun: function (req, res) {
            persistTestRunImpl(req, res);
        }
    };
})();