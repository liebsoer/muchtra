'use strict';

var express = require('express');
var controller = require('./question.controller');

var router = express.Router();

router.get('/', controller.getQuestions);
router.put('/:question', controller.addOrUpdateQuestion);
router.head('/:question', controller.isHasQuestion);
router.delete('/:question', controller.deleteQuestion);

module.exports = router;
